#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from sys import exit
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY


from cmd import utils
from cmd.utils import get_module_logger
from cmd.hdfs_namenode import NameNodeMetricCollector
from cmd.hdfs_datanode import DataNodeMetricCollector
from cmd.hdfs_journalnode import JournalNodeMetricCollector
from cmd.yarn_resourcemanager import ResourceManagerMetricCollector
from cmd.yarn_nodemanager import NodeManagerMetricCollector
from cmd.mapreduce_jobhistoryserver import MapReduceMetricCollector
from cmd.hbase_master import HBaseMasterMetricCollector
from cmd.hbase_regionserver import HBaseRegionServerMetricCollector
from cmd.hive_server import HiveServerMetricCollector
from cmd.hive_llap import HiveLlapDaemonMetricCollector

logger = get_module_logger(__name__)


def register_consul(address, port):
    start_http_server(port,addr=address)
    logger.info("Polling %s. Serving at port: %s" % (address, port))

def register_prometheus_a(cluster,dalist):
    dalist_grouped = {}
    for k in dalist:
      g=dalist_grouped.get(dalist[k]['type'])
      if g is None:
        dalist_grouped.update({dalist[k]['type']:[]})
        g=dalist_grouped.get(dalist[k]['type'])
      g.append(dalist[k])
    try:
        for k in dalist_grouped:
          v=dalist_grouped[k]
          logger.info("%s: %d instances:"%(k,len(v)))
          [logger.info("%s: %d. %s %s"%(k,i+1,v[i].get('instance'),v[i].get('url'))) for i in range(len(v))]
          if 'NAMENODE' == k:
            REGISTRY.register(NameNodeMetricCollector(cluster,v))
          elif 'DATANODE' == k:
            REGISTRY.register(DataNodeMetricCollector(cluster,v))
          elif 'JOURNALNODE' == k:
            REGISTRY.register(JournalNodeMetricCollector(cluster,v))
          elif 'RESOURCEMANAGER' == k:
            REGISTRY.register(ResourceManagerMetricCollector(cluster,v))
          elif 'NODEMANAGER' == k:
            REGISTRY.register(NodeManagerMetricCollector(cluster,v))
          elif 'HISTORYSERVER' == k:
            REGISTRY.register(MapReduceMetricCollector(cluster,v))
          elif 'HBASE_MASTER' == k:
            REGISTRY.register(HBaseMasterMetricCollector(cluster,v))
          elif 'HBASE_REGIONSERVER' == k:
            REGISTRY.register(HBaseRegionServerMetricCollector(cluster,v))
          elif 'HIVE_SERVER_INTERACTIVE' == k:
            REGISTRY.register(HiveServerMetricCollector(cluster,v))
          elif 'HIVE_LLAP' == k:
            REGISTRY.register(HiveLlapDaemonMetricCollector(cluster,v))
          else:
            logger.warn('unknown type %s'%k)
        while True:
            time.sleep(300)
    except KeyboardInterrupt:
        logger.info("Interrupted")
        exit(0)

def register_prometheus(rest_url):
    try:
        namenode_flag, datanode_flag, journalnode_flag, resourcemanager_flag, nodemanager_flag, hbase_master_flag, hbase_regionserver_flag, historyserver_flag, hive_server_interactive_flag, hive_llap_flag = 1,1,1,1,1,1,1,1,1,1
        while True:
            url = 'http://{0}/alert/getservicesbyhost'.format(rest_url)
            node_info = utils.get_node_info(url)
            if node_info:
                for cluster, info in node_info.items():
                    for k, v in info.items():
                        if 'NAMENODE' in k:
                            if namenode_flag:
                                namenode_url = v['jmx']
                                logger.info("namenode_url = {0}, start to register".format(namenode_url))
                                REGISTRY.register(NameNodeMetricCollector(cluster, namenode_url))
                                namenode_flag = 0
                                continue
                        if 'DATANODE' in k:
                            if datanode_flag:
                                datanode_url = v['jmx']
                                logger.info("datanode_url = {0}, start to register".format(datanode_url))
                                REGISTRY.register(DataNodeMetricCollector(cluster, datanode_url))
                                datanode_flag = 0
                                continue
                        if 'JOURNALNODE' in k:
                            if journalnode_flag:
                                journalnode_url = v['jmx']
                                logger.info("journalnode_url = {0}, start to register".format(journalnode_url))
                                REGISTRY.register(JournalNodeMetricCollector(cluster, journalnode_url))
                                journalnode_flag = 0
                                continue
                        if 'RESOURCEMANAGER' in k:
                            if resourcemanager_flag:
                                resourcemanager_url = v['jmx']
                                logger.info("resourcemanager_url = {0}, start to register".format(resourcemanager_url))
                                REGISTRY.register(ResourceManagerMetricCollector(cluster, resourcemanager_url))
                                resourcemanager_flag = 0
                                continue
                        if 'NODEMANAGER' in k:
                            if nodemanager_flag:
                                nodemanager_url = v['jmx']
                                logger.info("nodemanager_url = {0}, start to register".format(nodemanager_url))
                                REGISTRY.register(NodeManagerMetricCollector(cluster, nodemanager_url))
                                nodemanager_flag = 0
                                continue
                        if 'HBASE_MASTER' in k:
                            if hbase_master_flag:
                                hbase_master_url = v['jmx']
                                logger.info("hbase_master_url = {0}, start to register".format(hbase_master_url))
                                REGISTRY.register(HBaseMasterMetricCollector(cluster, hbase_master_url))
                                hbase_master_flag = 0
                                continue
                        if 'HBASE_REGIONSERVER' in k:
                            if hbase_regionserver_flag:
                                hbase_regionserver_url = v['jmx']
                                logger.info("hbase_regionserver_url = {0}, start to register".format(hbase_regionserver_url))
                                REGISTRY.register(HBaseRegionServerMetricCollector(cluster, hbase_regionserver_url))
                                hbase_regionserver_flag = 0
                                continue
                        if 'HISTORYSERVER' in k:
                            if historyserver_flag:
                                historyserver_url = v['jmx']
                                logger.info("historyserver_url = {0}, start to register".format(historyserver_url))
                                REGISTRY.register(MapReduceMetricCollector(cluster, historyserver_url))
                                historyserver_flag = 0
                                continue
                        if 'HIVE_SERVER_INTERACTIVE' in k:
                            if hive_server_interactive_flag:
                                hive_server_interactive_url = v['jmx']
                                logger.info("hive_server_interactive_url = {0}, start to register".format(hive_server_interactive_url))
                                REGISTRY.register(HiveServerMetricCollector(cluster, hive_server_interactive_url))
                                hive_server_interactive_flag = 0
                                continue
                        if 'HIVE_LLAP' in k:
                            if hive_llap_flag:
                                hive_llap_url = v['jmx']
                                logger.info("hive_llap_url = {0}, start to register".format(hive_llap_url))
                                REGISTRY.register(HiveLlapDaemonMetricCollector(cluster, hive_llap_url))
                                hive_llap_flag = 0
                                continue
                time.sleep(300)
            else:
                logger.error("No service running in THIS node")
                # if getserviceshost url return null dict, sleep 60s, retry to reconnect the api.
                time.sleep(300)

    except KeyboardInterrupt:
        print ("Interrupted")
        exit(0)
    except Exception as e:
        logger.info("Error in register prometheus, msg: {0}".format(e))
        pass

def main():
        args = utils.parse_args()
        address = args.address
        port = int(args.port)
        register_consul(address, port)
        if args.services_api is not None:
          register_prometheus(args.services)
        else:
          dalist={}
          if args.services_file is not None:
           logger.info("Loading service list from %s"%utils.os.path.abspath(args.services_file))
           try:
            with open(args.services_file) as fp:
              line = fp.readline()
              while line:
                t=line.split()
                if len(t)>=3:
                  #k='_'.join((t[0],t[1]))
                  k=t[2]
                  if k in dalist:
                    logger.warn("skip %s %s %s"%(t[0],t[1],t[2]))
                  else:
                    dalist.update({k:{'type':t[0],'instance':None if t[1]=='None' else t[1],'url':t[2]}})
                else:
                  logger.warn("skip line %s"%(line,))
                line = fp.readline()
           except Exception as e:
            logger.error("Failed loading %s: %s"%(utils.os.path.abspath(args.services_file),': '.join([str(s) for s in e.args])))
          for c,t in (
                      ('NAMENODE',args.namenode_url),
                      ('DATANODE',args.datanode_url),
                      ('JOURNALNODE',args.journalnode_url),
                      ('RESOURCEMANAGER',args.resourcemanager_url),
                      ('NODEMANAGER',args.nodemanager_url),
                      ('HBASE_MASTER',args.hbase_url),
                      ('HBASE_REGIONSERVER',args.regionserver_url),
                      ('HISTORYSERVER',args.mapreduce2_url),
                      ('HIVE_SERVER_INTERACTIVE',args.hive_url),
                      ('HIVE_LLAP',args.llapdaemon_url),
                      ):
              if t is not None:
                  k=t
                  if k in dalist:
                     logger.warn("replacing for %s %s@%s with %s"%(t,dalist[k]['type'],dalist[k]['instance'],c))
                  dalist.update({k:{'type':c,'instance':None,'url':t}})
          register_prometheus_a(args.cluster,dalist)

if __name__ == "__main__":
    main()
