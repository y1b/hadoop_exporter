#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from sys import exit
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from cmd.utils import get_module_logger, get_metrics_all, get_safe_name
from cmd.common import MetricCol, common_metrics_info

logger = get_module_logger(__name__)



class NodeManagerMetricCollector(MetricCol):

    def __init__(self, cluster, lst):
        MetricCol.__init__(self, cluster, lst, "yarn", "nodemanager")
        self._hadoop_nodemanager_metrics = None

    def collect(self):
        # Request data from ambari Collect Host API
        # Request exactly the System level information we need from node
        # beans returns a type of 'List'

        get_metrics_all(self._lst)
        for v in self._lst:
            self._hadoop_nodemanager_metrics = {}
            for i in range(len(self._file_list)):
                self._hadoop_nodemanager_metrics.setdefault(self._file_list[i], {})

            beans=self._setup_instance(v)
            # set up all metrics with labels and descriptions.
            self._setup_metrics_labels(beans)
    
            # add metric value to every metric.
            self._get_metrics(beans)
    
            # update namenode metrics with common metrics
            common_metrics = common_metrics_info(self, beans)
            self._hadoop_nodemanager_metrics.update(common_metrics())
    
            for i in range(len(self._merge_list)):
                service = self._merge_list[i]
                for metric in self._hadoop_nodemanager_metrics[service]:
                    yield self._hadoop_nodemanager_metrics[service][metric]

    def _setup_metrics_labels(self, beans):
        # The metrics we want to export.
        for i in range(len(beans)):
            label = self._label_names("host")
            for service in self._metrics:
                if service in beans[i]['name']:
                    for metric in self._metrics[service]:
                        self._hadoop_nodemanager_metrics[service][metric] = GaugeMetricFamily("_".join([self._prefix, get_safe_name(metric)]),
                                                                                              self._metrics[service][metric],
                                                                                              labels=label)
                else:
                    continue

    def _get_metrics(self, beans):
        for i in range(len(beans)):
            if 'tag.Hostname' in beans[i]:
                host = beans[i]['tag.Hostname']
                label = self._label_values(host)
                break
            else:
                continue
        for i in range(len(beans)):
            for service in self._metrics:
                if service in beans[i]['name']:
                    for metric in beans[i]:
                        if metric in self._metrics[service]:
                            self._hadoop_nodemanager_metrics[service][metric].add_metric(label, beans[i][metric])
                        else:
                            pass
                else:
                    continue

def main():
    try:
        args = utils.parse_args()
        port = int(args.port)
        cluster = args.cluster
        v = args.nodemanager_url
        REGISTRY.register(NodeManagerMetricCollector(cluster, v))

        start_http_server(port)
        # print("Polling %s. Serving at port: %s" % (args.address, port))
        print("Polling %s. Serving at port: %s" % (args.address, port))
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(" Interrupted")
        exit(0)


if __name__ == "__main__":
    main()
