#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml
import time
from sys import exit
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from cmd.utils import get_module_logger, get_metrics_all, get_snake_case
from cmd.common import MetricCol, common_metrics_info

logger = get_module_logger(__name__)

class DataNodeMetricCollector(MetricCol):
    def __init__(self, cluster, lst):
        MetricCol.__init__(self, cluster, lst, "hdfs", "datanode")
        self._hadoop_datanode_metrics = None

    def collect(self):
        # Request data from ambari Collect Host API
        # Request exactly the System level information we need from node
        # beans returns a type of 'List'

        get_metrics_all(self._lst)
        for v in self._lst:
            self._hadoop_datanode_metrics = {}
            for i in range(len(self._file_list)):
                self._hadoop_datanode_metrics.setdefault(self._file_list[i], {})

            beans=self._setup_instance(v)
            # set up all metrics with labels and descriptions.
            self._setup_metrics_labels(beans)
    
            # add metric value to every metric.
            self._get_metrics(beans)
    
            # update namenode metrics with common metrics
            common_metrics = common_metrics_info(self, beans)
            self._hadoop_datanode_metrics.update(common_metrics())
    
            for i in range(len(self._merge_list)):
                service = self._merge_list[i]
                for metric in self._hadoop_datanode_metrics[service]:
                    yield self._hadoop_datanode_metrics[service][metric]

    def _setup_dninfo_labels(self):
        for metric in self._metrics['DataNodeInfo']:
            if 'ActorState' in metric:
                label = self._label_names("version", "host")
                name = "_".join([self._prefix, 'actor_state'])
            elif 'VolumeInfo' in metric:
                label = self._label_names("version", "path", "state")
                name = "_".join([self._prefix, 'volume_state'])
            else:
                label = self._label_names("version")
                snake_case = get_snake_case(metric)
                name = "_".join([self._prefix, snake_case])
            self._hadoop_datanode_metrics['DataNodeInfo'][metric] = GaugeMetricFamily(name,
                                                                                      self._metrics['DataNodeInfo'][metric],
                                                                                      labels=label)
            
    def _setup_dnactivity_labels(self):
        block_flag, client_flag = 1, 1
        for metric in self._metrics['DataNodeActivity']:
            if 'Blocks' in metric:
                if block_flag:
                    label = self._label_names('host', 'oper')
                    key = "Blocks"
                    name = "block_operations_total"
                    descriptions = "Total number of blocks in different oprations"
                    block_flag = 0
                else:
                    continue
            elif 'Client' in metric:
                if client_flag:
                    label = self._label_names('host', 'oper', 'client')
                    key = "Client"
                    name = "from_client_total"
                    descriptions = "Total number of each operations from different client"
                    client_flag = 0
                else:
                    continue
            else:
                label = self._label_names('host')
                key = metric
                name = get_snake_case(metric)
                descriptions = self._metrics['DataNodeActivity'][metric]
            self._hadoop_datanode_metrics['DataNodeActivity'][key] = GaugeMetricFamily("_".join([self._prefix, name]),
                                                                                       descriptions,
                                                                                       labels=label)

    def _setup_dnvolume_labels(self):
        iorate_num_flag, iorate_avg_flag = 1, 1
        for metric in self._metrics['DataNodeVolume']:
            if 'IoRateNumOps' in metric:
                if iorate_num_flag:
                    label = self._label_names('host', 'path', 'oper')
                    iorate_num_flag = 0
                    key = "IoRateNumOps"
                    name = "file_io_operations_total"
                    descriptions = "The number of each file io operations within an interval time of metric"
                else:
                    continue
            elif 'IoRateAvgTime' in metric:
                if iorate_avg_flag:
                    label = self._label_names('host', 'path', 'oper')
                    iorate_avg_flag = 0
                    key = "IoRateAvgTime"
                    name = "file_io_operations_milliseconds"
                    descriptions = "Mean time of each file io operations in milliseconds"
                else:
                    continue
            else:
                label = self._label_names('host', 'path')
                key = metric
                descriptions = self._metrics['DataNodeVolume'][metric]
                if 'NumOps' in metric:
                    name = "_".join([get_snake_case(metric.split("NumOps")[0]), "total"])
                elif 'AvgTime' in metric:
                    name = "_".join([get_snake_case(metric.split("AvgTime")[0]), "time_milliseconds"])
                else:
                    name = get_snake_case(metric)
            self._hadoop_datanode_metrics['DataNodeVolume'][key] = GaugeMetricFamily("_".join([self._prefix, name]),
                                                                                     descriptions,
                                                                                     labels = label)
    
    def _setup_fsdatasetstate_labels(self):
        for metric in self._metrics['FSDatasetState']:
            label = self._label_names('host')
            if "Num" in metric:
                snake_case = get_snake_case(metric.split("Num")[1])
            else:
                snake_case = get_snake_case(metric)
            self._hadoop_datanode_metrics['FSDatasetState'][metric] = GaugeMetricFamily("_".join([self._prefix, snake_case]),
                                                                                        self._metrics['FSDatasetState'][metric],
                                                                                        labels = label)

    def _setup_metrics_labels(self, beans):
        # The metrics we want to export.
        for i in range(len(beans)):
            if 'DataNodeInfo' in beans[i]['name']:
                self._setup_dninfo_labels()
            if 'DataNodeActivity' in self._metrics:
                self._setup_dnactivity_labels()
            if 'DataNodeVolume' in self._metrics:
                self._setup_dnvolume_labels()
            if 'FSDatasetState' in self._metrics:
                self._setup_fsdatasetstate_labels()

    def _get_dninfo_metrics(self, bean):
        for metric in self._metrics['DataNodeInfo']:
            version = bean['Version']
            if 'ActorState' in metric:
                if 'BPServiceActorInfo' in bean:
                    actor_info_list = yaml.safe_load(bean['BPServiceActorInfo'])
                    for j in range(len(actor_info_list)):
                        host = actor_info_list[j]['NamenodeAddress'].split(':')[0]
                        label = self._label_values(version, host)
                        if 'state' == "RUNNING":
                            value = 1.0
                        else:
                            value = 0.0
                        self._hadoop_datanode_metrics['DataNodeInfo'][metric].add_metric(label, value)
                else:
                    continue
            elif 'VolumeInfo' in metric:
                if 'VolumeInfo' in bean:
                    volume_info_dict = yaml.safe_load(bean['VolumeInfo'])
                    for k, v in volume_info_dict.items():
                        path = k
                        for key, val in v.items():
                          if key!='storageType':
                            state = key
                            label = self._label_values(version, path, state)
                            value = val
                            self._hadoop_datanode_metrics['DataNodeInfo'][metric].add_metric(label, value)
                else:
                    continue
            else:
                label = self._label_values(version)
                value = bean[metric]
                self._hadoop_datanode_metrics['DataNodeInfo'][metric].add_metric(label, value)

    def _get_dnactivity_metrics(self, bean):
        for metric in self._metrics['DataNodeActivity']:
          if metric in bean:
            host = bean['tag.Hostname']
            label = self._label_values(host)
            if 'Blocks' in metric:
                oper = metric.split("Blocks")[1]
                label.append(oper)
                key = "Blocks"
            elif 'Client' in metric:
                oper = metric.split("Client")[0].split("From")[0]
                client = metric.split("Client")[0].split("From")[1]
                label.extend([oper, client])
                key = "Client"
            else:
                key = metric
            self._hadoop_datanode_metrics['DataNodeActivity'][key].add_metric(label,bean[metric])

    def _get_dnvolume_metrics(self, bean):
        for metric in self._metrics['DataNodeVolume']:
          if metric in bean:
            host = bean['tag.Hostname']
            path = bean['name'].split("-")[1]
            label = self._label_values(host, path)
            if 'IoRateNumOps' in metric:
                oper = metric.split("IoRateNumOps")[0]
                label.append(oper)
                key = "IoRateNumOps"
            elif 'IoRateAvgTime' in metric:
                oper = metric.split("IoRateAvgTime")[0]
                label.append(oper)
                key = "IoRateAvgTime"
            else:
                key = metric
            self._hadoop_datanode_metrics['DataNodeVolume'][key].add_metric(label,bean[metric])

    def _get_fsdatasetstate_metrics(self, bean):
        for metric in self._metrics['FSDatasetState']:
          if metric in bean:
            host = bean['tag.Hostname']
            label = self._label_values(host)
            self._hadoop_datanode_metrics['FSDatasetState'][metric].add_metric(label, bean[metric])

    def _get_metrics(self, beans):
        for i in range(len(beans)):
            if 'DataNodeInfo' in beans[i]['name']:
                self._get_dninfo_metrics(beans[i])
            if 'DataNodeActivity' in beans[i]['name']:
                self._get_dnactivity_metrics(beans[i])
            #if 'DataNodeVolume' in beans[i]['name']:
            #    self._get_dnvolume_metrics(beans[i])
            if 'FSDatasetState' in beans[i]['name'] and 'FSDatasetState' in beans[i]['modelerType']:
                self._get_fsdatasetstate_metrics(beans[i])
                    
def main():
    try:
        port = 9131
        host = '127.0.0.1'
        cluster = None
        lst = [
             {'url':'http://10.110.13.101:50075/jmx', 'instance':'NODE01'},
             {'url':'http://10.110.13.101:50075/jmx'},
             {'url':'http://10.110.13.102:50075/jmx', 'instance':'NODE03'},
             {'url':'http://10.110.13.103:50075/jmx', 'instance':'NODE03'},
            ]
        #for v in lst:
        #    if v.get('instance') is None:
        #        v.update({'instance':'instancelabel'})
        for i in range(len(lst)):
            print(i,lst[i])
        REGISTRY.register(DataNodeMetricCollector(cluster, lst))

        start_http_server(port,addr=host)
        # print("Polling %s. Serving at port: %s" % (args.address, port))
        print("Polling %s. Serving at port: %s" % (host, port))
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(" Interrupted")
        exit(0)


if __name__ == "__main__":
    main()
