#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from sys import exit
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY

from cmd.utils import get_module_logger, get_metrics_all, get_safe_name
from cmd.common import MetricCol, common_metrics_info

logger = get_module_logger(__name__)

class MapReduceMetricCollector(MetricCol):

    def __init__(self, cluster, lst):
        MetricCol.__init__(self, cluster, lst, "mapreduce", "jobhistoryserver")
        self._hadoop_jobhistoryserver_metrics = None

    def collect(self):
        # Request data from ambari Collect Host API
        # Request exactly the System level information we need from node
        # beans returns a type of 'List'

        get_metrics_all(self._lst)
        for v in self._lst:
            self._hadoop_jobhistoryserver_metrics = {}
            for i in range(len(self._file_list)):
                 self._hadoop_jobhistoryserver_metrics.setdefault(self._file_list[i], {})

            beans=self._setup_instance(v)
            # set up all metrics with labels and descriptions.
            self._setup_metrics_labels()
    
            # add metric value to every metric.
            self._get_metrics(beans)
    
            # update namenode metrics with common metrics
            common_metrics = common_metrics_info(self, beans)
            self._hadoop_jobhistoryserver_metrics.update(common_metrics())
    
            for i in range(len(self._merge_list)):
                service = self._merge_list[i]
                for metric in self._hadoop_jobhistoryserver_metrics[service]:
                    yield self._hadoop_jobhistoryserver_metrics[service][metric]



def main():
    try:
        args = utils.parse_args()
        port = int(args.port)
        cluster = args.cluster
        v = args.mapreduce2_url
        REGISTRY.register(MapReduceMetricCollector(cluster, v))

        start_http_server(port)
        # print("Polling %s. Serving at port: %s" % (args.address, port))
        print("Polling %s. Serving at port: %s" % (args.address, port))
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(" Interrupted")
        exit(0)


if __name__ == "__main__":
    main()
