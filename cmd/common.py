#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from prometheus_client.core import GaugeMetricFamily

from cmd import utils
from cmd.utils import get_module_logger, get_snake_case

logger = get_module_logger(__name__)


class MetricCol(object):
    '''
    MetricCol is a super class of all kinds of MetricsColleter classes. It setup common params like cluster, url, component and service.
    '''
    def __init__(self, cluster,lst, component, service):
        '''
        @param cluster: Cluster name, registered in the config file or ran in the command-line.
        @param url: All metrics are scraped in the url, corresponding to each component. 
                    e.g. "hdfs" metrics can be scraped in http://ip:50070/jmx.
                         "resourcemanager" metrics can be scraped in http://ip:8088/jmx.
        @param component: Component name. e.g. "hdfs", "resourcemanager", "mapreduce", "hive", "hbase".
        @param service: Service name. e.g. "namenode", "resourcemanager", "mapreduce".
        '''
        self._cluster = cluster
        self._instance = None
        self._lst = lst
        self._component = component
        self._prefix = 'hadoop_{0}_{1}'.format(component, service)

        self._file_list = utils.get_file_list(service)
        self._common_file = utils.get_file_list("common")
        self._merge_list = self._file_list + self._common_file

        self._service = service
        self._metrics = {}
        for i in range(len(self._file_list)):
            self._metrics.setdefault(self._file_list[i], utils.read_json_file(self._service, self._file_list[i]))

    def collect(self):
        '''
        This method needs to be override by all subclasses.

        # request_data from url/jmx
        metrics = get_metrics(self._base_url)
        beans = metrics['beans']

        # initial the metircs
        self._setup_metrics_labels()

        # add metrics
        self._get_metrics(beans)
        '''
        pass

    def _setup_metrics_labels(self):
        pass

    def _get_metrics(self, metrics):
        pass

    def _setup_instance(self,v):
        beans=v['beans']
        instance=v.get('instance')
        hasforcedinstance=instance is not None
        if hasforcedinstance is False:
            host = None
            for i in range(len(beans)):
                if 'tag.Hostname' in beans[i]:
                    host = beans[i]['tag.Hostname']
                    break
                else:
                    continue
            instance=host if host is not None else urlparse(v['url']).hostname
        self._instance=instance
        return v['beans']

    def _label_values(self,*args):
        r = []
        if self._cluster is not None:
            r.append(self._cluster)
        if self._instance is not None:
            r.append(self._instance)
        r.extend(args)
        return r

    def _label_names(self,*args):
        r = []
        if self._cluster is not None:
            r.append("cluster")
        if self._instance is not None:
            r.append("instance")
        r.extend(args)
        return r


#static methods and variables
_metrics_type = utils.get_file_list("common")
tmp_metrics = {}
for i in range(len(_metrics_type)):
    tmp_metrics.setdefault(_metrics_type[i], utils.read_json_file("common", _metrics_type[i]))

def common_metrics_info(cls, beans):
    '''
    A closure function was setup to scrape the SAME metrics all services have.
    @return a closure variable named common_metrics, which contains all the metrics that scraped from the given beans.
    '''
    common_metrics = {}

    for i in range(len(_metrics_type)):
        common_metrics.setdefault(_metrics_type[i], {})

    def setup_jvm_labels():
        for metric in tmp_metrics["JvmMetrics"]:
            '''
            Processing module JvmMetrics
            '''
            snake_case = "_".join(["jvm", get_snake_case(metric)])
            if 'Mem' in metric:
                label = cls._label_names("mode")
                if "Used" in metric:
                    key = "jvm_mem_used_bytes"
                    descriptions = "Current memory used in bytes."
                elif "Committed" in metric:
                    key = "jvm_mem_committed_bytes"
                    descriptions = "Current memory committed in bytes."
                elif "Max" in metric:
                    key = "jvm_mem_max_bytes"
                    descriptions = "Current max memory in bytes."
                else:
                    key = utils.re.sub(r'm$', 'bytes', snake_case)
                    label = cls._label_names()
                    descriptions = tmp_metrics['JvmMetrics'][metric]
            elif 'Gc' in metric:
                label = cls._label_names("type")
                if "GcCount" in metric:
                    key = "jvm_gc_count"
                    descriptions = "GC count of each type GC."
                elif "GcTimeMillis" in metric:
                    key = "jvm_gc_time_milliseconds"
                    descriptions = "Each type GC time in milliseconds."
                elif "ThresholdExceeded" in metric:
                    key = "jvm_gc_exceeded_threshold_total"
                    descriptions = "Number of times that the GC threshold is exceeded."
                else:
                    key = snake_case
                    label = cls._label_names()
                    descriptions = tmp_metrics['JvmMetrics'][metric]
            elif 'Threads' in metric:
                label = cls._label_names("state")
                key = "jvm_threads_state_total"
                descriptions = "Current number of different threads."
            elif 'Log' in metric:
                label = cls._label_names("level")
                key = "jvm_log_level_total"
                descriptions = "Total number of each level logs."
            else:
                label = cls._label_names()
                key = snake_case
                descriptions = tmp_metrics['JvmMetrics'][metric]
            common_metrics['JvmMetrics'][key] = GaugeMetricFamily("_".join([cls._prefix, key]),
                                                                  descriptions,
                                                                  labels=label)
        return common_metrics

    def setup_os_labels():
        for metric in tmp_metrics['OperatingSystem']:
            label = cls._label_names()
            common_metrics['OperatingSystem'][metric] = GaugeMetricFamily("_".join([cls._prefix, get_snake_case(metric)]),
                                                                          tmp_metrics['OperatingSystem'][metric],
                                                                          labels=label)
        return common_metrics

    def setup_rpc_labels():
        num_rpc_flag, avg_rpc_flag = 1, 1
        for metric in tmp_metrics["RpcActivity"]:
            '''
            Processing module RpcActivity, when multiple RpcActivity module exist, 
            `tag.port` should be an identifier to distinguish each module.
            '''
            if 'Rpc' in metric:
                snake_case = get_snake_case(metric)
            else:
                snake_case = "_".join(["rpc", get_snake_case(metric)])
            label = cls._label_names("tag")
            if "NumOps" in metric:
                if num_rpc_flag:
                    key = "MethodNumOps"
                    label.append("method")
                    common_metrics['RpcActivity'][key] = GaugeMetricFamily("_".join([cls._prefix, "rpc_method_called_total"]),
                                                                           "Total number of the times the method is called.",
                                                                           labels = label)
                    num_rpc_flag = 0
                else:
                    continue
            elif "AvgTime" in metric:
                if avg_rpc_flag:
                    key = "MethodAvgTime"
                    label.append("method")
                    common_metrics['RpcActivity'][key] = GaugeMetricFamily("_".join([cls._prefix, "rpc_method_avg_time_milliseconds"]),
                                                                           "Average turn around time of the method in milliseconds.",
                                                                           labels = label)
                    avg_rpc_flag = 0
                else:
                    continue
            else:
                key = metric
                common_metrics['RpcActivity'][key] = GaugeMetricFamily("_".join([cls._prefix, snake_case]),
                                                                       tmp_metrics['RpcActivity'][metric],
                                                                       labels = label)
        return common_metrics    
    
    def setup_rpc_detailed_labels():
        for metric in tmp_metrics['RpcDetailedActivity']:
            label = cls._label_names("tag")
            if "NumOps" in metric:
                key = "NumOps"
                label.append("method")
                name = "_".join([cls._prefix, 'rpc_detailed_method_called_total'])
            elif "AvgTime" in metric:
                key = "AvgTime"
                label.append("method")
                name = "_".join([cls._prefix, 'rpc_detailed_method_avg_time_milliseconds'])
            else:
                pass
            common_metrics['RpcDetailedActivity'][key] = GaugeMetricFamily(name,
                                                                           tmp_metrics['RpcDetailedActivity'][metric],
                                                                           labels = label)
        return common_metrics

    def setup_ugi_labels():
        done_flags=[]
        for metric in tmp_metrics['UgiMetrics']:
            label = cls._label_names()
            m = utils.re_ugi_AvgTime_or_NumOps.match(metric)
            if m is not None:
                label.extend(("method","state"))
                method,state,key=m.groups()
                if key=='NumOps':
                    name="ugi_method_called_total"
                    desc="Total number of the times the method is called."
                elif key=='AvgTime':
                    name="ugi_method_avg_time_milliseconds"
                    desc="Average turn around time of the method in milliseconds."
                else:
                    name="_".join("ugi_method",key.lower())
                    desc=key
            else:
                name="_".join(("ugi",get_snake_case(metric)))
                key=metric
                desc=tmp_metrics['UgiMetrics'][metric]
            if name not in done_flags:
                common_metrics['UgiMetrics'][key] = GaugeMetricFamily("_".join([cls._prefix, name]),desc,labels=label)
                done_flags.append(name)
        return common_metrics

    def setup_metric_system_labels():
        metric_num_flag, metric_avg_flag = 1, 1
        for metric in tmp_metrics['MetricsSystem']:
            label = cls._label_names()
            if 'NumOps' in metric:
                if metric_num_flag:
                    key = 'NumOps'
                    label.append("oper")
                    metric_num_flag = 0
                    common_metrics['MetricsSystem'][key] = GaugeMetricFamily("_".join([cls._prefix, 'metricssystem_operations_total']),
                                                                             "Total number of operations",
                                                                             labels = label)
                else:
                    continue
            elif 'AvgTime' in metric:
                if metric_avg_flag:
                    key = 'AvgTime'
                    label.append("oper")
                    metric_avg_flag = 0
                    common_metrics['MetricsSystem'][key] = GaugeMetricFamily("_".join([cls._prefix, 'metricssystem_method_avg_time_milliseconds']),
                                                                             "Average turn around time of the operations in milliseconds.",
                                                                             labels = label)
                else:
                    continue
            else:
                common_metrics['MetricsSystem'][metric] = GaugeMetricFamily("_".join([cls._prefix, 'metricssystem', get_snake_case(metric)]),
                                                                            tmp_metrics['MetricsSystem'][metric],
                                                                            labels = label)
        return common_metrics

    def setup_runtime_labels():
        for metric in tmp_metrics['Runtime']:
            label = cls._label_names("host")
            common_metrics['Runtime'][metric] = GaugeMetricFamily("_".join([cls._prefix, get_snake_case(metric)]),
                                                                  tmp_metrics['Runtime'][metric], 
                                                                  labels = label)
        return common_metrics

    def setup_scrape_labels():
        for metric in tmp_metrics['ScrapeStatus']:
            label = cls._label_names()
            common_metrics['ScrapeStatus'][metric] = GaugeMetricFamily("_".join([cls._prefix, get_snake_case(metric)]),
                                                                  tmp_metrics['ScrapeStatus'][metric],
                                                                  labels = label)
        return common_metrics

    def setup_labels(beans):
        '''
        预处理，分析各个模块的特点，进行分类，添加label
        '''
        for i in range(len(beans)):
            if 'name=JvmMetrics' in beans[i]['name']:
                setup_jvm_labels()

            elif 'OperatingSystem' in beans[i]['name']:
                setup_os_labels()

            elif 'RpcActivity' in beans[i]['name']:
                setup_rpc_labels()

            elif 'RpcDetailedActivity' in beans[i]['name']:
                setup_rpc_detailed_labels()

            elif 'UgiMetrics' in beans[i]['name']:
                setup_ugi_labels()

            elif 'MetricsSystem' in beans[i]['name'] and "sub=Stats" in beans[i]['name']:
                setup_metric_system_labels()   

            elif 'Runtime' in beans[i]['name']:
                setup_runtime_labels()

            elif 'ScrapeStatus' in beans[i]['name']:
                setup_scrape_labels()
                
        return common_metrics



    def get_jvm_metrics(bean):
        for metric in tmp_metrics['JvmMetrics']:
          if metric in bean:
            name = "_".join(["jvm", get_snake_case(metric)])
            val=bean[metric] if metric in bean else 0
            if 'Mem' in metric:
                val=round(1024*1024*val) if val>0 else val
                if "Used" in metric:
                    key = "jvm_mem_used_bytes"
                    mode = metric.split("Used")[0].split("Mem")[1]
                    label = cls._label_values(mode)
                elif "Committed" in metric:
                    key = "jvm_mem_committed_bytes"
                    mode = metric.split("Committed")[0].split("Mem")[1]
                    label = cls._label_values(mode)
                elif "Max" in metric:
                    key = "jvm_mem_max_bytes"
                    if "Heap" in metric:
                        mode = metric.split("Max")[0].split("Mem")[1]
                    else:
                        mode = "max"
                    label = cls._label_values(mode)
                else:
                    key = utils.re.sub(r'm$', 'bytes', name)
                    label = cls._label_values()
                
            elif 'Gc' in metric:
                if "GcCount" in metric:
                    key = "jvm_gc_count"
                    if "GcCount" == metric:
                        typo = "total"
                    else:
                        typo = metric.split("GcCount")[1]
                    label = cls._label_values(typo)
                elif "GcTimeMillis" in metric:
                    key = "jvm_gc_time_milliseconds"
                    if "GcTimeMillis" == metric:
                        typo = "total"
                    else:
                        typo = metric.split("GcTimeMillis")[1]
                    label = cls._label_values(typo)
                elif "ThresholdExceeded" in metric:
                    key = "jvm_gc_exceeded_threshold_total"
                    typo = metric.split("ThresholdExceeded")[0].split("GcNum")[1]
                    label = cls._label_values(typo)
                else:
                    key = name
                    label = cls._label_values()
            elif 'Threads' in metric:
                key = "jvm_threads_state_total"
                state = metric.split("Threads")[1]
                label = cls._label_values(state)
            elif 'Log' in metric:
                key = "jvm_log_level_total"
                level = metric.split("Log")[1]
                label = cls._label_values(level)
            else:
                key = name
                label = cls._label_values()
            common_metrics['JvmMetrics'][key].add_metric(label,val)
        return common_metrics

    def get_os_metrics(bean):
        for metric in tmp_metrics['OperatingSystem']:
          if metric in bean:
            label = cls._label_values()
            val=bean[metric]/1e9 if metric=='ProcessCpuTime' else bean[metric]
            common_metrics['OperatingSystem'][metric].add_metric(label,val)
        return common_metrics

    def get_rpc_metrics(bean):
        rpc_tag = bean['tag.port']
        for metric in tmp_metrics['RpcActivity']:
            if "NumOps" in metric:
                method = metric.split('NumOps')[0]
                label = cls._label_values(rpc_tag, method)
                key = "MethodNumOps"
            elif "AvgTime" in metric:
                method = metric.split('AvgTime')[0]
                label = cls._label_values(rpc_tag, method)
                key = "MethodAvgTime"
            else:
                label = cls._label_values(rpc_tag)
                key = metric
            common_metrics['RpcActivity'][key].add_metric(label,
                                                          bean[metric] if metric in bean else 0)
        return common_metrics

    def get_rpc_detailed_metrics(bean):
        detail_tag = bean['tag.port']
        for metric in bean:
            if metric[0].isupper():
                label = cls._label_values(detail_tag)
                if "NumOps" in metric:
                    key = "NumOps"
                    method = metric.split('NumOps')[0]
                    label = cls._label_values(detail_tag, method)
                elif "AvgTime" in metric:
                    key = "AvgTime"
                    method = metric.split("AvgTime")[0]
                    label = cls._label_values(detail_tag, method)
                else:
                    pass
                common_metrics['RpcDetailedActivity'][key].add_metric(label,
                                                                      bean[metric])
        return common_metrics

    def get_ugi_metrics(bean):
        for metric in tmp_metrics['UgiMetrics']:
          if metric in bean:
            label = cls._label_values()
            m = utils.re_ugi_AvgTime_or_NumOps.match(metric)
            if m is not None:
                method,state,key=m.groups()
                label.extend(('' if method is None else method, '' if state is None else state))
            else:
                key=metric
            common_metrics['UgiMetrics'][key].add_metric(label, bean[metric] if bean[metric] else 0)
        return common_metrics

    def get_metric_system_metrics(bean):
        for metric in tmp_metrics['MetricsSystem']:
            if 'NumOps' in metric:
                key = 'NumOps'
                oper = metric.split('NumOps')[0]
                label = cls._label_values(oper)
            elif 'AvgTime' in metric:
                key = 'AvgTime'
                oper = metric.split('AvgTime')[0]
                label = cls._label_values(oper)
            else:
                key = metric
                label = cls._label_values()
            common_metrics['MetricsSystem'][key].add_metric(label, bean[metric] if metric in bean and bean[metric] else 0)
        return common_metrics

    def get_runtime_metrics(bean):
        for metric in tmp_metrics['Runtime']:
            label = cls._label_values(bean['Name'].split("@")[1])
            common_metrics['Runtime'][metric].add_metric(label, bean[metric]/1000 if metric in bean and bean[metric] else 0)
        return common_metrics

    def get_scrape_metrics(bean):
        for metric in tmp_metrics['ScrapeStatus']:
            label = cls._label_values()
            common_metrics['ScrapeStatus'][metric].add_metric(label, bean[metric] if metric in bean and bean[metric] else 0)
        return common_metrics

    def get_metrics():
        '''
        给setup_labels模块的输出结果进行赋值，从url中获取对应的数据，挨个赋值
        '''
        common_metrics = setup_labels(beans)
        for i in range(len(beans)):
            if 'name=JvmMetrics' in beans[i]['name']:
                get_jvm_metrics(beans[i])

            if 'OperatingSystem' in beans[i]['name']:
                get_os_metrics(beans[i])

            if 'RpcActivity' in beans[i]['name']:
                get_rpc_metrics(beans[i])

            if 'RpcDetailedActivity' in beans[i]['name']:
                get_rpc_detailed_metrics(beans[i])

            if 'UgiMetrics' in beans[i]['name']:
                get_ugi_metrics(beans[i])

            if 'MetricsSystem' in beans[i]['name'] and "sub=Stats" in beans[i]['name']:
                get_metric_system_metrics(beans[i])

            if 'Runtime' in beans[i]['name']:
                get_runtime_metrics(beans[i])

            if 'ScrapeStatus' in beans[i]['name']:
                get_scrape_metrics(beans[i])

        return common_metrics

    return get_metrics
