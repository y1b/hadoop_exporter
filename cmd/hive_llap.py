#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
from sys import exit
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from cmd.utils import get_module_logger, get_metrics_all, get_safe_name
from cmd.common import MetricCol, common_metrics_info

logger = get_module_logger(__name__)

class HiveLlapDaemonMetricCollector(MetricCol):

    def __init__(self, cluster, lst):
        MetricCol.__init__(self, cluster, lst, "hive", "llapdaemon")
        self._hadoop_llapdaemon_metrics = None

    def collect(self):
        # Request data from ambari Collect Host API
        # Request exactly the System level information we need from node
        # beans returns a type of 'List'

        get_metrics_all(self._lst)
        for v in self._lst:
            self._hadoop_llapdaemon_metrics = {}
            for i in range(len(self._file_list)):
                self._hadoop_llapdaemon_metrics.setdefault(self._file_list[i], {})

            beans=self._setup_instance(v)
            # set up all metrics with labels and descriptions.
            self._setup_labels(beans)
    
            # add metric value to every metric.
            self._get_metrics(beans)
    
            # update namenode metrics with common metrics
            common_metrics = common_metrics_info(self, beans)
            self._hadoop_llapdaemon_metrics.update(common_metrics())
    
            for i in range(len(self._merge_list)):
                service = self._merge_list[i]
                for metric in self._hadoop_llapdaemon_metrics[service]:
                    yield self._hadoop_llapdaemon_metrics[service][metric]

    def _setup_executor_labels(self, bean, service):
        for metric in self._metrics[service]:
            if metric in bean:
                if "ExecutorThread" in metric:
                    label = self._label_names('host', 'cpu')
                else:
                    label = self._label_names('host')
                self._hadoop_llapdaemon_metrics[service][metric] = GaugeMetricFamily("_".join([self._prefix, service.lower(), get_safe_name(metric)]),
                                                                                      self._metrics[service][metric],
                                                                                      labels=label)
            else:
                continue

    def _setup_other_labels(self, bean, service):
        label = self._label_names("host")
        for metric in self._metrics[service]:
            if metric in bean:
                self._hadoop_llapdaemon_metrics[service][metric] = GaugeMetricFamily("_".join([self._prefix, service.lower(), get_safe_name(metric)]),
                                                                                      self._metrics[service][metric],
                                                                                      labels=label)
            else:
                continue

    def _setup_labels(self, beans):
        # The metrics we want to export.
        for service in self._metrics:
            for i in range(len(beans)):
                if 'LlapDaemonExecutorMetrics' == service and 'LlapDaemonExecutorMetrics' in beans[i]['name']:
                    self._setup_executor_labels(beans[i], service)
                elif service in beans[i]['name']:
                    self._setup_other_labels(beans[i], service)
                else:
                    continue

    def _get_executor_metrics(self, bean, service, host):
        for metric in bean:
            if metric in self._metrics[service]:
                if "ExecutorThread" in metric:
                    cpu = "".join(['cpu', metric.split("_")[1]])
                    self._hadoop_llapdaemon_metrics[service][metric].add_metric(self._label_values(host, cpu), bean[metric])
                else:
                    self._hadoop_llapdaemon_metrics[service][metric].add_metric(self._label_values(host), bean[metric])
            else:
                continue

    def _get_other_metrics(self, bean, service, host):
        for metric in bean:
            if metric in self._metrics[service]:
                self._hadoop_llapdaemon_metrics[service][metric].add_metric(self._label_values(host), bean[metric])
            else:
                continue

    def _get_metrics(self, beans):
        # bean is a type of <Dict>
        # status is a type of <Str>
        for i in range(len(beans)):
            if 'tag.Hostname' in beans[i]:
                host = beans[i]['tag.Hostname']
                break
            else:
                continue
        for i in range(len(beans)):
            for service in self._metrics:
                if 'LlapDaemonExecutorMetrics' == service and 'LlapDaemonExecutorMetrics' in beans[i]['name']:
                    self._get_executor_metrics(beans[i], service, host)
                elif service in beans[i]['name']:
                    self._get_other_metrics(beans[i], service, host)
                else:
                    continue

def main():
    try:
        args = utils.parse_args()
        port = int(args.port)
        cluster = args.cluster
        v = args.llapdaemon_url
        REGISTRY.register(HiveLlapDaemonMetricCollector(cluster, v))

        start_http_server(port)
        # print("Polling %s. Serving at port: %s" % (args.address, port))
        print("Polling %s. Serving at port: %s" % (args.address, port))
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(" Interrupted")
        exit(0)


if __name__ == "__main__":
    main()
