#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import socket
import requests
import argparse
import logging
import yaml
import re

import time
from threading import Thread

re_snake_case = re.compile(r'([a-z0-9])([A-Z])')
re_safe_name = re.compile('[^a-z0-9A-Z]')
re_ugi_AvgTime_or_NumOps = re.compile(r'^(.*?)(Failure|Success)?(AvgTime|NumOps)$')

def get_snake_case(s):
    return re_snake_case.sub(r'\1_\2', s).lower()

def get_safe_name(s):
    return re_safe_name.sub('_', s).lower()

def get_module_logger(mod_name):
    '''
    define a common logger template to record log.
    @param mod_name log module name.
    @return logger.
    '''
    logger = logging.getLogger(mod_name)
    logger.setLevel(logging.DEBUG)
    # 设置终端输出handler，并设置记录级别
    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)

    # 设置日志格式
    fmt = logging.Formatter(fmt='%(asctime)s %(filename)s[line:%(lineno)d]-[%(levelname)s]: %(message)s')
    sh.setFormatter(fmt)

    # 添加handler到logger对象
    logger.addHandler(sh)
    return logger

logger = get_module_logger(__name__)

def get_metrics_all(lst):
    def get_metrics_t(t,i):
        if os.environ.get('HTTP_PROXY') is not None:
            time.sleep(0.02*i) #delay few milisecnds to avoid chocking proxy
        t[i].update({'beans':get_metrics(t[i]['url'])})
        return True
    num=len(lst)
    threads = [Thread(target=get_metrics_t, args=[lst, i]) for i in range(num)]
    [th.start() for th in threads]
    [th.join() for th in threads]

def get_metrics(url):
    '''
    :param url: The jmx url, e.g. http://host1:50070/jmx,http://host1:8088/jmx, http://host2:19888/jmx...
    :return a dict of all metrics scraped in the jmx url.
    '''
    result = []
    ts=time.time()
    try:
        response=requests.get(url,
                              headers={'Connection': 'close', 'Accept-Encoding':''},
                              timeout=(2, 10))
    except Exception as e:
        logger.warning("error in func: get_metrics, error msg: %s"%e)
        result = []
    else:
        if response.status_code != requests.codes.ok:
            logger.warning("Get {0} failed, response code is: {1}.".format(url, response.status_code))
            result = []
        else:
            try:
              rlt = response.json()
            except:
              logger.warning("Parse json {0} failed, response code is: {1}.".format(url, response.status_code))
              result = []
            else:
              logger.debug(rlt)
              if rlt and "beans" in rlt:
                result = rlt['beans']
              else:
                logger.warning("No metrics get in the {0}.".format(url))
                result = []
    finally:
        result.append({
          'name':'dummy=ScrapeStatus',
          'ScrapeDurationSeconds':(time.time()-ts),
          'ScrapeStartTime':ts,
          'ScrapeSuccess':1 if len(result)>0 else 0
        })
    return result

def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip

def get_hostname():
    '''
    get hostname via socket.
    @return a string of hostname
    '''
    try:
        host = socket.getfqdn()
    except Exception as e:
        logger.info("get hostname failed, error msg: {0}".format(e))
        return None
    else:
        return host

def read_json_file(path_name, file_name):
    '''
    read metric json files.
    '''
    path = os.path.dirname(os.path.realpath(__file__))
    parent_path = os.path.dirname(path)
    metric_path = os.path.join(parent_path, path_name)
    metric_name = "{0}.json".format(file_name)
    try:
        with open(os.path.join(metric_path, metric_name), 'r') as f:
            metrics = yaml.safe_load(f)
            return metrics
    except Exception as e:
        logger.info("read metrics json file failed, error msg is: %s" %e)
        return {}

def get_file_list(file_path_name):
    '''
    This function is to get all .json file name in the specified file_path_name.
    @param file_path: The file path name, e.g. namenode, ugi, resourcemanager ...
    @return a list of file name.
    '''
    path = os.path.dirname(os.path.abspath(__file__))
    parent_path = os.path.dirname(path)
    json_path = os.path.join(parent_path, file_path_name)
    try:
        files = os.listdir(json_path)
    except OSError:
        logger.info("No such file or directory: '%s'" %json_path)
        return []
    else:
        rlt = []
        for i in range(len(files)):
          if files[i].endswith('.json'):
            rlt.append(files[i].split(".json")[0])
        return rlt


def get_node_info(url):
    '''
    Firstly, I know how many nodes in the cluster.
    Secondly, exporter was installed in every node in the cluster.
    Therefore, how many services were installed in each node should be a key factor.
    In this function, a list of node info, including hostname, service jmx url should be returned.
    '''
    url = url.rstrip()
    host = get_hostname()
    node_info = {}
    try:
        s = requests.session()
        response = s.get(url, timeout = 120)
    except Exception as e:
        logger.info("Error happened while requests url {0}, error msg : {1}".format(url, e))
        node_info = {}
    else:
        if response.status_code != requests.codes.ok:
            logger.info("Get {0} failed, response code is: {1}.".format(url, response.status_code))
            node_info = {}
        result = response.json()
        logger.debug(result)
        if result:
            for k,v in result.items():
                for i in range(len(v)):
                    if host in v[i]:
                        node_info.setdefault(k, v[i][host])
                    else:
                        continue
            logger.debug(node_info)
        else:
            logger.info("No metrics get in the {0}.".format(url))
            node_info = {}
    finally:
        s.close()
    return node_info


def parse_args():

    parser = argparse.ArgumentParser(
        description = 'hadoop node exporter args, including url, metrics_path, address, port and cluster.'
    )
    parser.add_argument(
        '-c','--cluster',
        required = False,
        metavar = 'cluster_name',
        help = 'Hadoop cluster label. (default None)',
        default = None
    )
    parser.add_argument(
        '-s','--services-api',
        required = False,
        metavar = 'services_api',
        help = 'Services api to scrape each hadoop jmx url. (ex "127.0.0.1:9035")',
        default = None
    )
    parser.add_argument(
        '-f','--services-file',
        required = False,
        metavar = 'services_file',
        help = 'File with jmx url list to scrape \n===\nSERVICE1 INST001 http://host:port/jmx\nSERVICE2 INST002 http://host:port/jmx\n...\n===',
        default = None
    )
    parser.add_argument(
        '-hdfs', '--namenode-url',
        required=False,
        metavar='namenode_jmx_url',
        help='Hadoop hdfs metrics URL. (ex "http://indata-10-110-13-115.indata.com:50070/jmx")',
        default=None
    )
    parser.add_argument(
        '-rm', '--resourcemanager-url',
        required=False,
        metavar='resourcemanager_jmx_url',
        help='Hadoop resourcemanager metrics URL. (ex "http://indata-10-110-13-116.indata.com:8088/jmx")',
        default=None
    )
    parser.add_argument(
        '-dn', '--datanode-url',
        required=False,
        metavar='datanode_jmx_url',
        help='Hadoop datanode metrics URL. (ex "http://indata-10-110-13-114.indata.com:1022/jmx")',
        default=None
    )
    parser.add_argument(
        '-jn', '--journalnode-url',
        required=False,
        metavar='journalnode_jmx_url',
        help='Hadoop journalnode metrics URL. (ex "http://indata-10-110-13-114.indata.com:8480/jmx")',
        default=None
    )
    parser.add_argument(
        '-mr', '--mapreduce2-url',
        required=False,
        metavar='mapreduce2_jmx_url',
        help='Hadoop mapreduce2 metrics URL. (ex "http://indata-10-110-13-116.indata.com:19888/jmx")',
        default=None
    )
    parser.add_argument(
        '-nm', '--nodemanager-url',
        required=False,
        metavar='nodemanager_jmx_url',
        help='Hadoop nodemanager metrics URL. (ex "http://indata-10-110-13-114.indata.com:8042/jmx")',
        default=None
    )
    parser.add_argument(
        '-hbase', '--hbase-url',
        required=False,
        metavar='hbase_jmx_url',
        help='Hadoop hbase metrics URL. (ex "http://indata-10-110-13-115.indata.com:16010/jmx")',
        default=None
    )
    parser.add_argument(
        '-rs', '--regionserver-url',
        required=False,
        metavar='regionserver_jmx_url',
        help='Hadoop regionserver metrics URL. (ex "http://indata-10-110-13-114.indata.com:60030/jmx")',
        default=None
    )
    parser.add_argument(
        '-hive', '--hive-url',
        required=False,
        metavar='hive_jmx_url',
        help='Hadoop hive metrics URL. (ex "http://indata-10-110-13-116.indata.com:10502/jmx")',
        default=None
    )
    parser.add_argument(
        '-ld', '--llapdaemon-url',
        required=False,
        metavar='llapdaemon_jmx_url',
        help='Hadoop llapdaemon metrics URL. (ex "http://indata-10-110-13-116.indata.com:15002/jmx")',
        default=None
    )
    parser.add_argument(
        '-p','--path',
        metavar='metrics_path',
        required=False,
        help='Path under which to expose metrics. (default "/metrics")',
        default='/metrics'
    )
    parser.add_argument(
        '-host','-ip','--address','--addr',
        metavar='ip_or_hostname',
        required=False,
        type=str,
        help='Polling server on this address. (default "127.0.0.1")',
        default='127.0.0.1'
    )
    parser.add_argument(
        '-P', '--port',
        metavar='port',
        required=False,
        type=int,
        help='Listen to this port. (default "9131")',
        default=9131
    )
    return parser.parse_args()


def main():

    print (parse_args())
    print (get_file_list("namenode"))
    print (get_file_list("resourcemanager"))
    print ("==============================")
    url = "http://10.10.6.10:9035/alert/getservicesbyhost"
    print (get_node_info(url))
    print ("==============================")
    print (get_file_list("common"))
    pass

if __name__ == '__main__':
    main()
