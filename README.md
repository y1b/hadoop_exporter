# Hadoop Exporter for Prometheus
Exports hadoop metrics via HTTP for Prometheus consumption.

How to run
```
python hadoop_exporter.py
```

Help on flags of hadoop_exporter:
```
usage: hadoop_exporter.py [-h] [-c cluster_name] [-s services_api]
                          [-f services_file] [-hdfs namenode_jmx_url]
                          [-rm resourcemanager_jmx_url] [-dn datanode_jmx_url]
                          [-jn journalnode_jmx_url] [-mr mapreduce2_jmx_url]
                          [-nm nodemanager_jmx_url] [-hbase hbase_jmx_url]
                          [-rs regionserver_jmx_url] [-hive hive_jmx_url]
                          [-ld llapdaemon_jmx_url] [-p metrics_path]
                          [-host ip_or_hostname] [-P port]

hadoop node exporter args, including url, metrics_path, address, port and
cluster.

optional arguments:
  -h, --help            show this help message and exit
  -c cluster_name, --cluster cluster_name
                        Hadoop cluster label. (default empty)
  -s services_api, --services-api services_api
                        Services api to scrape each hadoop jmx url. (ex
                        "127.0.0.1:9035")
  -f services_file, --services-file services_file
                        File with jmx url list to scrape === SERVICE1 INST001
                        http://host:port/jmx SERVICE2 INST002
                        http://host:port/jmx ... ===
  -hdfs namenode_jmx_url, --namenode-url namenode_jmx_url
                        Hadoop hdfs metrics URL. (ex
                        "http://indata-10-110-13-115.indata.com:50070/jmx")
  -rm resourcemanager_jmx_url, --resourcemanager-url resourcemanager_jmx_url
                        Hadoop resourcemanager metrics URL. (ex
                        "http://indata-10-110-13-116.indata.com:8088/jmx")
  -dn datanode_jmx_url, --datanode-url datanode_jmx_url
                        Hadoop datanode metrics URL. (ex
                        "http://indata-10-110-13-114.indata.com:1022/jmx")
  -jn journalnode_jmx_url, --journalnode-url journalnode_jmx_url
                        Hadoop journalnode metrics URL. (ex
                        "http://indata-10-110-13-114.indata.com:8480/jmx")
  -mr mapreduce2_jmx_url, --mapreduce2-url mapreduce2_jmx_url
                        Hadoop mapreduce2 metrics URL. (ex
                        "http://indata-10-110-13-116.indata.com:19888/jmx")
  -nm nodemanager_jmx_url, --nodemanager-url nodemanager_jmx_url
                        Hadoop nodemanager metrics URL. (ex
                        "http://indata-10-110-13-114.indata.com:8042/jmx")
  -hbase hbase_jmx_url, --hbase-url hbase_jmx_url
                        Hadoop hbase metrics URL. (ex
                        "http://indata-10-110-13-115.indata.com:16010/jmx")
  -rs regionserver_jmx_url, --regionserver-url regionserver_jmx_url
                        Hadoop regionserver metrics URL. (ex
                        "http://indata-10-110-13-114.indata.com:60030/jmx")
  -hive hive_jmx_url, --hive-url hive_jmx_url
                        Hadoop hive metrics URL. (ex
                        "http://indata-10-110-13-116.indata.com:10502/jmx")
  -ld llapdaemon_jmx_url, --llapdaemon-url llapdaemon_jmx_url
                        Hadoop llapdaemon metrics URL. (ex
                        "http://indata-10-110-13-116.indata.com:15002/jmx")
  -p metrics_path, --path metrics_path
                        Path under which to expose metrics. (default
                        "/metrics")
  -host ip_or_hostname, -ip ip_or_hostname, --address ip_or_hostname, --addr ip_or_hostname
                        Polling server on this address. (default "127.0.0.1")
  -P port, --port port  Listen to this port. (default "9131")
```

example of services file (option -f)
```
NAMENODE NODE10 http://10.110.13.110:50070/jmx
NAMENODE NODE11 http://10.110.13.111:50070/jmx
DATANODE NODE01 http://10.110.13.101:50075/jmx
DATANODE NODE02 http://10.110.13.102:50075/jmx
DATANODE NODE03 http://10.110.13.103:50075/jmx
JOURNALNODE NODE10 http://10.110.13.110:8480/jmx
JOURNALNODE NODE11 http://10.110.13.111:8480/jmx
JOURNALNODE NODE01 http://10.110.13.101:8480/jmx
RESOURCEMANAGER NODE10 http://10.110.13.110:8088/jmx
RESOURCEMANAGER NODE11 http://10.110.13.111:8088/jmx
NODEMANAGER NODE01 http://10.110.13.101:8042/jmx
NODEMANAGER NODE02 http://10.110.13.102:8042/jmx
NODEMANAGER NODE03 http://10.110.13.103:8042/jmx
HISTORYSERVER NODE11 http://10.110.13.111:19888/jmx
HBASE_MASTER NODE10 http://10.110.13.110:16010/jmx
HBASE_MASTER NODE11 http://10.110.13.111:16010/jmx
HBASE_REGIONSERVER NODE01 http://10.110.13.101:16030/jmx
HBASE_REGIONSERVER NODE02 http://10.110.13.102:16030/jmx
HBASE_REGIONSERVER NODE03 http://10.110.13.103:16030/jmx
HIVE_SERVER_INTERACTIVE NODE10 http://10.110.13.110:10502/jmx
HIVE_LLAP NODE10 http://10.110.13.110:15002/jmx
```

Tested on Apache Hadoop 2.x and 3.x
# hadoop_exporter
